<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190620205935 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, image_id INT DEFAULT NULL, url_id VARCHAR(100) NOT NULL, url_slug VARCHAR(100) NOT NULL, heading VARCHAR(100) NOT NULL, subtitle VARCHAR(100) NOT NULL, introduction VARCHAR(100) NOT NULL, display_date VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_23A0E66F675F31B (author_id), UNIQUE INDEX UNIQ_23A0E663DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_author (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_image (id INT AUTO_INCREMENT NOT NULL, height VARCHAR(100) NOT NULL, width VARCHAR(100) NOT NULL, text VARCHAR(100) NOT NULL, url VARCHAR(100) NOT NULL, source VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chapter (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, sort_order INT NOT NULL, headline VARCHAR(100) NOT NULL, text VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_F981B52E3DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chapter_image (id INT AUTO_INCREMENT NOT NULL, height VARCHAR(100) NOT NULL, width VARCHAR(100) NOT NULL, text VARCHAR(100) NOT NULL, url VARCHAR(100) NOT NULL, source VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66F675F31B FOREIGN KEY (author_id) REFERENCES article_author (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E663DA5256D FOREIGN KEY (image_id) REFERENCES article_image (id)');
        $this->addSql('ALTER TABLE chapter ADD CONSTRAINT FK_F981B52E3DA5256D FOREIGN KEY (image_id) REFERENCES chapter_image (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66F675F31B');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E663DA5256D');
        $this->addSql('ALTER TABLE chapter DROP FOREIGN KEY FK_F981B52E3DA5256D');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_author');
        $this->addSql('DROP TABLE article_image');
        $this->addSql('DROP TABLE chapter');
        $this->addSql('DROP TABLE chapter_image');
    }
}
