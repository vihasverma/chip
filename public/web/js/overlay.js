var image = document.querySelectorAll("img");
var i;
for (i = 0; i < image.length; i++) {
    image[i].addEventListener("click", function (evt) {

        var overlay = document.createElement("div");
        overlay.style.width = (window.innerWidth) + 'px';
        overlay.style.height = (window.innerHeight) + 'px';
        overlay.style.background = "rgba(0, 0, 0, 0.4)";
        overlay.style.position = "absolute";
        overlay.style.left = 0 + "px";
        overlay.style.top = 0 + "px";
        overlay.style.float = "left";
        overlay.style.position = "absolute";

        var largeimg = document.createElement("img");
        largeimg.src = evt.target.src;
        largeimg.style.width = 800 + "px";
        largeimg.style.height = 500 + "px";
        var x = (window.innerWidth - 800) / 2;
        var y = (window.innerHeight - 500) / 2;
        largeimg.style.position = "absolute";
        largeimg.style.left = x + "px";
        largeimg.style.top = y + "px";
        overlay.appendChild(largeimg);
        document.body.appendChild(overlay);
        largeimg.addEventListener("click", function (evt1) {

            if (overlay) {
                overlay.parentNode.removeChild(overlay);
            }

        });
    });
}