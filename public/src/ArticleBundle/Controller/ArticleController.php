<?php

namespace ArticleBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ArticleBundle\Services\ArticleService;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * @Route("/article", service="controller.article")
 */
class ArticleController
{
    private $ArticleService;
    private $twig;
	
    public function __construct(
        ArticleService $ArticleService,
        Environment $twig
    )
    {
        $this->ArticleService = $ArticleService;
        $this->twig = $twig;
    }

    /**
     * @Route("/import", name="import")
     */
    public function importAction()
    {
        $form = $this->ArticleService->importArticle();
        $content = $this->twig->render('@Article/import.html.twig', [
            'form' => $form]);
        return new Response($content);

    }
	
    /**
     * @Route("/list", name="list")
     */
    public function listAction()
    {
        $articles = $this->ArticleService->listArticle();
        $content = $this->twig->render('@Article/list.html.twig', [
            'articles' => $articles]);
        return new Response($content);

    }
}