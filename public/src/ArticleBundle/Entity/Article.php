<?php

namespace ArticleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 *
 * @ORM\Entity(repositoryClass="ArticleBundle\Entity\ArticleRepository")
 * @ORM\Table(name="article")
 */
class Article 
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;
	
	/**
	* @ORM\Column(type="string", length=100)
	*/
    private $urlId;

	/**
	* @ORM\Column(type="string", length=100)
	*/
    private $urlSlug;	
	
	/**
	* @ORM\Column(type="string", length=100)
	*/
    private $heading;
	
   /**
	* @ORM\Column(type="string", length=100)
	*/
    private $subtitle;
	
    /**
	* @ORM\Column(type="string", length=100)
	*/
    private $introduction;
	
    /**
	* @ORM\Column(type="string", length=100)
	*/
    private $displayDate;	
	
    /**
     * @ORM\OneToOne(targetEntity="ArticleAuthor", cascade={"persist"})
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;    
	
	/**
     * @ORM\OneToOne(targetEntity="ArticleImage",  cascade={"persist"})
	 * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;
	
   /**
     * @ORM\OneToMany(targetEntity="Chapter", mappedBy="article",  cascade={"persist"})
	 * 
     */
    private $chapters;
	
	
	 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chaperts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set urlId
     *
     * @param string $urlId
     *
     * @return Article
     */
    public function setUrlId($urlId)
    {
        $this->urlId = $urlId;

        return $this;
    }

    /**
     * Get urlId
     *
     * @return string
     */
    public function getUrlId()
    {
        return $this->urlId;
    }

    /**
     * Set urlSlug
     *
     * @param string $urlSlug
     *
     * @return Article
     */
    public function setUrlSlug($urlSlug)
    {
        $this->urlSlug = $urlSlug;

        return $this;
    }

    /**
     * Get urlSlug
     *
     * @return string
     */
    public function getUrlSlug()
    {
        return $this->urlSlug;
    }

    /**
     * Set heading
     *
     * @param string $heading
     *
     * @return Article
     */
    public function setHeading($heading)
    {
        $this->heading = $heading;

        return $this;
    }

    /**
     * Get heading
     *
     * @return string
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return Article
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set introduction
     *
     * @param string $introduction
     *
     * @return Article
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;

        return $this;
    }

    /**
     * Get introduction
     *
     * @return string
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * Set displayDate
     *
     * @param string $displayDate
     *
     * @return Article
     */
    public function setDisplayDate($displayDate)
    {
        $this->displayDate = $displayDate;

        return $this;
    }

    /**
     * Get displayDate
     *
     * @return string
     */
    public function getDisplayDate()
    {
        return $this->displayDate;
    }

    /**
     * Set author
     *
     * @param \ArticleBundle\Entity\ArticleAuthor $author
     *
     * @return Article
     */
    public function setAuthor(\ArticleBundle\Entity\ArticleAuthor $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \ArticleBundle\Entity\ArticleAuthor
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set image
     *
     * @param \ArticleBundle\Entity\ArticleImage $image
     *
     * @return Article
     */
    public function setImage(\ArticleBundle\Entity\ArticleImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \ArticleBundle\Entity\ArticleImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add chapert
     *
     * @param \ArticleBundle\Entity\Chapter $chapert
     *
     * @return Article
     */
    public function addChapert(\ArticleBundle\Entity\Chapter $chapert)
    {
        $this->chaperts[] = $chapert;

        return $this;
    }

    /**
     * Remove chapert
     *
     * @param \ArticleBundle\Entity\Chapter $chapert
     */
    public function removeChapert(\ArticleBundle\Entity\Chapter $chapert)
    {
        $this->chaperts->removeElement($chapert);
    }

    /**
     * Get chaperts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChaperts()
    {
        return $this->chaperts;
    }

    /**
     * Add chapter
     *
     * @param \ArticleBundle\Entity\Chapter $chapter
     *
     * @return Article
     */
    public function addChapter(\ArticleBundle\Entity\Chapter $chapter)
    {
        $this->chapters[] = $chapter;

        return $this;
    }

    /**
     * Remove chapter
     *
     * @param \ArticleBundle\Entity\Chapter $chapter
     */
    public function removeChapter(\ArticleBundle\Entity\Chapter $chapter)
    {
        $this->chapters->removeElement($chapter);
    }

    /**
     * Get chapters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChapters()
    {
        return $this->chapters;
    }
}
