<?php

namespace ArticleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 *
 * @ORM\Entity(repositoryClass="ArticleBundle\Entity\ChapterRepository")
 * @ORM\Table(name="chapter")
 */
class Chapter 
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;

	
	/**
	* @ORM\Column(type="integer", length=11)
	*/
    private $sort_order;

	/**
	* @ORM\Column(type="string", length=100)
	*/
    private $headline;	
	
	/**
	* @ORM\Column(type="string", length=100)
	*/
    private $text;
	
	/**
     * @ORM\OneToOne(targetEntity="ChapterImage",  cascade={"persist"})
	 * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;	
	 

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Chapter
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set headline
     *
     * @param string $headline
     *
     * @return Chapter
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;

        return $this;
    }

    /**
     * Get headline
     *
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Chapter
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set article
     *
     * @param \ArticleBundle\Entity\Article $article
     *
     * @return Chapter
     */
    public function setArticle(\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \ArticleBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set image
     *
     * @param \ArticleBundle\Entity\ChapterImage $image
     *
     * @return Chapter
     */
    public function setImage(\ArticleBundle\Entity\ChapterImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \ArticleBundle\Entity\ChapterImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Chapter
     */
    public function setSortOrder($sortOrder)
    {
        $this->sort_order = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sort_order;
    }
}
