<?php
namespace ArticleBundle\Services\Article;


use ArticleBundle\Entity\Article;

class ArticleDetail extends ArticleDecorator
{


    public function __construct(
        ArticleInterface $articleComponent
    )
    {
        parent::__construct($articleComponent);
    }

    public function process(ArticleData $ArticleData)
    {
        $this->articleComponent->process($ArticleData);
        $this->addDetail($ArticleData);
    }

    private function addDetail(ArticleData $ArticleData)
    {
        $articleJson = $ArticleData->getJson();
        $article = new Article();
        $article->setUrlId($articleJson['urlId'])
            ->setAuthor($ArticleData->getArticleAuthor())
            ->setUrlSlug($articleJson['urlSlug'])
            ->setHeading($articleJson['headline'])
            ->setDisplayDate($articleJson['displayDate']['timestamp'])
            ->setImage($ArticleData->getArticleImage())
            ->setIntroduction($articleJson['introduction'])
            ->setSubtitle($articleJson['subtitle']);
        $ArticleData->setArticleDetail($article);
    }
}
