<?php
namespace ArticleBundle\Services\Article;


class ArticleImage extends ArticleDecorator
{


    public function __construct(
        ArticleInterface $articleComponent
    )
    {
        parent::__construct($articleComponent);
    }

    public function process(ArticleData $ArticleData)
    {
        $this->articleComponent->process($ArticleData);
        $this->addImage($ArticleData);
    }

    private function addImage(ArticleData $ArticleData)
    {
        $articleJson = $ArticleData->getJson();
        $articleImage = new \ArticleBundle\Entity\ArticleImage();
        $articleImage->setHeight($articleJson['image']['height'])
            ->setSource($articleJson['image']['source'])
            ->setText($articleJson['image']['text'])
            ->setUrl($articleJson['image']['url'])
            ->setWidth($articleJson['image']['width']);
        $ArticleData->setArticleImage($articleImage);
    }
}
