<?php
namespace ArticleBundle\Services\Article;

interface ArticleInterface
{
    public function process(ArticleData $data);
}
