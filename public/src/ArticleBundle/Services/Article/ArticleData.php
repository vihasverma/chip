<?php
namespace ArticleBundle\Services\Article;

class ArticleData
{
   private $json;
    private $articleAuthor;
    private $articleDetail;
    private $articleImage;

    /**
     * @return mixed
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @param mixed $json
     */
    public function setJson($json)
    {
        $this->json = $json;
    }

    /**
     * @return mixed
     */
    public function getArticleAuthor()
    {
        return $this->articleAuthor;
    }

    /**
     * @param mixed $articleAuthor
     */
    public function setArticleAuthor($articleAuthor)
    {
        $this->articleAuthor = $articleAuthor;
    }

    /**
     * @return mixed
     */
    public function getArticleDetail()
    {
        return $this->articleDetail;
    }

    /**
     * @param mixed $articleDetail
     */
    public function setArticleDetail($articleDetail)
    {
        $this->articleDetail = $articleDetail;
    }

    /**
     * @return mixed
     */
    public function getArticleImage()
    {
        return $this->articleImage;
    }

    /**
     * @param mixed $articleImage
     */
    public function setArticleImage($articleImage)
    {
        $this->articleImage = $articleImage;
    }

}
