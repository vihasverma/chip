<?php
namespace ArticleBundle\Services\Article;



use ArticleBundle\Services\Chapter\ChapterData;
use ArticleBundle\Services\Chapter\ChapterInterface;

class ArticleChapter extends ArticleDecorator
{


    public function __construct(
        ArticleInterface $articleComponent,
        ChapterInterface $chapterDecorator
    )
    {
        $this->chapterDecorator = $chapterDecorator;
        parent::__construct($articleComponent);
    }

    public function process(ArticleData $ArticleData)
    {
        $this->articleComponent->process($ArticleData);
        $this->addChapter($ArticleData);
    }

    private function addChapter(ArticleData $ArticleData)
    {
        $articleJson = $ArticleData->getJson();
        $article = $ArticleData->getArticleDetail();
        foreach($articleJson['chapters'] as $chapterJson) {
            $chapterData = new ChapterData();
            $chapterData->setJson($chapterJson);
            $this->chapterDecorator->process($chapterData);
            $article->addChapter($chapterData->getChapterDetail());
        }
        $ArticleData->setArticleDetail($article);
    }
}
