<?php
namespace ArticleBundle\Services\Article;

class ArticleAuthor extends ArticleDecorator
{
    public function __construct(
        ArticleInterface $articleComponent
    )
    {
        parent::__construct($articleComponent);
    }

    public function process(ArticleData $ArticleData)
    {
        $this->articleComponent->process($ArticleData);
        $this->addAuthor($ArticleData);
    }

    private function addAuthor(ArticleData $ArticleData)
    {
        $articleJson = $ArticleData->getJson();
        $articleAuthor = new \ArticleBundle\Entity\ArticleAuthor();
        $articleAuthor->setFirstName($articleJson['author']['firstName'])
            ->setLastName($articleJson['author']['lastName']);
        $ArticleData->setArticleAuthor($articleAuthor);
    }
}
