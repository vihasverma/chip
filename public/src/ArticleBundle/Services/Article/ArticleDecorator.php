<?php

namespace ArticleBundle\Services\Article;

abstract class ArticleDecorator implements ArticleInterface
{
    protected $articleComponent;

    public function __construct(ArticleInterface $articleComponent)
    {
        $this->articleComponent = $articleComponent;
    }

    abstract public function process(ArticleData $data);

}
