<?php
namespace ArticleBundle\Services\Article;


use Doctrine\ORM\EntityManager;

class ArticlePersist extends ArticleDecorator
{
    private $em;
    public function __construct(
        ArticleInterface $articleComponent,
        EntityManager $em
    )
    {
        $this->em = $em;
        parent::__construct($articleComponent);
    }

    public function process(ArticleData $ArticleData)
    {
        $this->articleComponent->process($ArticleData);
        $this->addArticle($ArticleData);
    }

    private function addArticle(ArticleData $ArticleData)
    {
        $this->em->persist($ArticleData->getArticleDetail());
        $this->em->flush();
    }
}
