<?php
namespace ArticleBundle\Services;

use ArticleBundle\Entity\ArticleRepository;
use ArticleBundle\Form\Type\ArticleImportType;
use ArticleBundle\Services\Article\ArticleData;
use ArticleBundle\Services\Article\ArticleInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class ArticleService
{
    private $form;
    private $request;
    private $error;
    private $articleRepository;

    public function __construct(
        FormFactory $form,
        RequestStack $requestStack,
        ArticleInterface $articleDecorator,
        ArticleRepository $articleRepository
    )
    {

        $this->form = $form;
        $this->request = $requestStack->getCurrentRequest();
        $this->articleDecorator = $articleDecorator;
        $this->articleRepository = $articleRepository;
    }

    public function importArticle()
    {
        $form = $this->form->create(ArticleImportType::class);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $articleJson = $this->parseJson(file_get_contents($article['json']->getPathName()));
            if ($this->error != "") {
                $form->addError(new FormError($this->error));
                return $form->createView();
            }
            $articleData = new ArticleData();

            $articleData->setJson($articleJson);
            $this->articleDecorator->process($articleData);
        }
        return $form->createView();
    }
    public function listArticle(){
        return $this->articleRepository->findAll();
    }
    private function parseJson($rawJson)
    {
        $jsonDecode = json_decode($rawJson, true);

        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                $this->error = ' - Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $this->error = ' - Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $this->error = ' - Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $this->error = ' - Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $this->error = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
        }
        return $jsonDecode;

    }
}