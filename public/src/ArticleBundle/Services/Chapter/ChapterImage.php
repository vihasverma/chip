<?php
namespace ArticleBundle\Services\Chapter;


class ChapterImage extends  ChapterDecorator
{
    public function __construct(
        ChapterInterface $chapterComponent
    )
    {
        parent::__construct($chapterComponent);
    }

    public function process(ChapterData $chapterData)
    {
        $this->chapterComponent->process($chapterData);
        $this->addImage($chapterData);
    }

    private function addImage(ChapterData $chapterData)
    {
        $chapterJson = $chapterData->getJson();
        if(count($chapterJson['image'])>0){
            $chapterImage = new \ArticleBundle\Entity\ChapterImage();
            $chapterImage->setHeight($chapterJson['image']['height'])
                ->setSource($chapterJson['image']['source'])
                ->setText($chapterJson['image']['text'])
                ->setUrl($chapterJson['image']['url'])
                ->setWidth($chapterJson['image']['width']);
            $chapterData->setChapterImage($chapterImage);
        }


    }
}
