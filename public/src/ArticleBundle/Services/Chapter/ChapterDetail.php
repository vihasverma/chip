<?php
namespace ArticleBundle\Services\Chapter;


use ArticleBundle\Entity\Chapter;

class ChapterDetail extends ChapterDecorator
{


    public function __construct(
        ChapterInterface $chapterComponent
    )
    {
        parent::__construct($chapterComponent);
    }

    public function process(ChapterData $chapterData)
    {
        $this->chapterComponent->process($chapterData);
        $this->addDetail($chapterData);
    }

    private function addDetail(ChapterData $chapterData)
    {
        $chapter = new Chapter();
        $chapterJson = $chapterData->getJson();
        $chapter->setText($chapterJson['text'])
            ->setHeadline($chapterJson['headline'])

            ->setSortOrder($chapterJson['order']);
        if(null != $chapterData->getChapterImage()){
            $chapter->setImage($chapterData->getChapterImage());
        }
        $chapterData->setChapterDetail($chapter);
    }
}
