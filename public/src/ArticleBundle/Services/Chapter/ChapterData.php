<?php
namespace ArticleBundle\Services\Chapter;

class ChapterData
{
   private $json;
    private $chapterDetail;
    private $chapterImage;

    /**
     * @return mixed
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @param mixed $json
     */
    public function setJson($json)
    {
        $this->json = $json;
    }

    /**
     * @return mixed
     */
    public function getChapterDetail()
    {
        return $this->chapterDetail;
    }

    /**
     * @param mixed $chapterDetail
     */
    public function setChapterDetail($chapterDetail)
    {
        $this->chapterDetail = $chapterDetail;
    }

    /**
     * @return mixed
     */
    public function getChapterImage()
    {
        return $this->chapterImage;
    }

    /**
     * @param mixed $chapterImage
     */
    public function setChapterImage($chapterImage)
    {
        $this->chapterImage = $chapterImage;
    }


}
