<?php

namespace ArticleBundle\Services\Chapter;

abstract class ChapterDecorator implements ChapterInterface
{
    protected $chapterComponent;

    public function __construct(ChapterInterface $chapterComponent)
    {
        $this->chapterComponent = $chapterComponent;
    }

    abstract public function process(ChapterData $data);

}
