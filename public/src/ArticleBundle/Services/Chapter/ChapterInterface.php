<?php
namespace ArticleBundle\Services\Chapter;

interface ChapterInterface
{
    public function process(ChapterData $data);
}
