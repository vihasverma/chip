<?php
namespace tests\PhpunitBundle\Controller;
use ArticleBundle\Controller\ArticleController;
use ArticleBundle\Services\ArticleService;
use PHPUnit\Framework\TestCase;

class ArticleControllerTest extends TestCase
{
    private $articleServiceMock;
    private $articleController;
    private $twig;

    protected function setUp()
    {

        $this->twig = $this->getMockBuilder(\Twig_Environment::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->articleServiceMock = $this->getMockBuilder(ArticleService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->articleController = new ArticleController(
            $this->articleServiceMock, $this->twig
        );
    }

    protected function tearDown()
    {
        $this->articleServiceMock = null;
    }

    public function testImportAction()
    {
        $this->articleServiceMock
            ->expects($this->once())
            ->method('importArticle');

        $this->articleController->importAction();
    }


    public function testListAction()
    {
        $this->articleServiceMock
            ->expects($this->once())
            ->method('listArticle');

        $this->articleController->listAction();

    }



}