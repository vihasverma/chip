# Chip Coding Challenge: Article Json Import 
>Submitted By: Vihas Verma

From provided JSON files write a small service which imports the files and stores the data in a local DB.
Read the data from the DB to render a microsite which displays the article in a simple layout.




### Installation


 I tried to use docker for the environment purpose but as my system had only 4 gb ram and it was becoming too slow so I could not use it. 
 Please use the following steps for application installation:


```sh

$ git clone https://vihasverma@bitbucket.org/vihasverma/chip.git

$ cd chip

$ cd public

$ composer install

```

Please enter the following details as per your environment during the proccess of composer install prompt

```sh			

database_host (127.0.0.1):
database_port (null):
database_name (symfony): chip_database44
database_user (root):
database_password (null):

```
I have used doctrine migration bundle to create the datatbase schema so please run the following command to setup database and then start server.

```sh

php bin/console doctrine:migrations:execute 20190620205935

php bin/console server:run

```

##URL for article listing page: 

(http://localhost:8000/article/list) You may also click the import button to reach json import page

##URL for article iport page:

(http://localhost:8000/article/import) 


## Code Design


I have used decorator design pattern for importing the articles, author, image and chapers and tried to follow solid pricliples
 so that in future if we need to add new properties in article we can be easily add it to decorator using new class without affecting others.

```sh
 
      article.decorator:
        class: ArticleBundle\Services\Article\Article
        
    article.article_image:
        class: ArticleBundle\Services\Article\ArticleImage
        public: false
        decorates: article.decorator
        decoration_priority: -1
        arguments:
            - "@article.article_image.inner"
			
	article.article_author:
		class: ArticleBundle\Services\Article\ArticleAuthor
		public: false
		decorates: article.decorator
		decoration_priority: -2
		arguments:

```		
			
* I have used controller as service to follow best practice 
* Used Form builder to create import form.
* Twig to display the import and listing page
* I have create my own overlay javascipt as you requested which execute when user click on the thumbnail on article listing page.(/web/js/overlay.js)
* For demo purpose I also created some unit test for the controller. Due to less time I couldnot write test for the services   
 
 
 
 